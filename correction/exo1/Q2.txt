PROCEDURE f(n : ENTIER) :
VAR 
    i, j : ENTIER
    F, G : Tableau d'ENTIER
DEBUT
    SI n < 1 ALORS
        AFFICHER "f(", n, ") n'est pas défini"
    SINON SI n = 1 ALORS
        AFFICHER "f(", n, ") = 1"
    SINON
        F <- [1]
        G <- [2]
        TANT QUE LONGUEUR(F) < n FAIRE
            i <- LONGUEUR(G) + 1
            SI LONGUEUR(F) < i ALORS
                AJOUTER max(F[DERNIER INDEX de F], G[DERNIER INDEX de G]) + 1 À F
            FIN SI
            j <- F[i]
            SI LONGUEUR(F) < j ALORS
                POUR loop ALLANT DE 1 À (j - LONGUEUR(F)) FAIRE
                    AJOUTER max(F[DERNIER INDEX de F], G[DERNIER INDEX de G]) + 1 À F
                FIN POUR
            FIN SI
            AJOUTER F[j] + 1 À G
        FIN TANT QUE

        AFFICHER "f(", n, ") =", F[n]
    FIN SI
FIN PROCEDURE
