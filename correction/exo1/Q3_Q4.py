# Q3
def f(n):
    if n < 1:
        print(f"f({n}) n'est pas défini")
    elif n == 1:
        print(f"f({n}) = 1")
    else:
        F = [1]
        G = [2]
        while len(F) < n:
            i = len(G) + 1
            if len(F) < i:
                F.append(max(F[-1], G[-1]) + 1)
            j = F[i - 1]
            if len(F) < j:
                for loop in range(j - len(F)):
                    F.append(max(F[-1], G[-1]) + 1)
            G.append(F[j - 1] + 1)

        print(f"f({n}) = {F[n - 1]}")

if __name__ == "__main__":
    # Q4
    f(2023)
