def C(n, p):
    if p < 0 or n < 0 or p > n:
        print(f"C({n}, {p}) n'est pas défini")
        return
    elif p == 0 or p == n:
        return 1
    elif p == 1:
        return n
    else:
        return C(n-1, p-1) + C(n-1, p)


if __name__ == "__main__":
    print(C(5, 2))
