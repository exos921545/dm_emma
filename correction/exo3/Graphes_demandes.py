import timeit
import matplotlib.pyplot as plt
from Q3_Python import *
from Q2 import *

if __name__ == "__main__":
    # Mesurer le temps d'exécution pour différentes tailles de tableau
    tailles = [100 * i**2 for i in range(11)]

    tab_temps_tri_insertion = []
    for taille in tailles:
        A = list(range(taille, 0, -5))  # Pire cas
        temps = timeit.timeit(lambda: tri_insertion(A.copy()), number=4)
        tab_temps_tri_insertion.append(temps)

    tab_temps_tri_selection = []
    for taille in tailles:
        A = list(range(taille, 0, -5))  # Pire cas
        temps = timeit.timeit(lambda: tri_selection(A.copy()), number=4)
        tab_temps_tri_selection.append(temps)

    # Tracer les courbe
    plt.plot(tailles, tab_temps_tri_insertion, label="Tri insertion")
    plt.plot(tailles, tab_temps_tri_selection, label="Tri sélection")
    plt.xlabel("Taille n du tableau")
    plt.ylabel("Temps d'exécution en (s)")
    plt.legend()
    plt.show()
