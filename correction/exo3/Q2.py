def tri_selection(A):
    n = len(A)
    if n < 2:
        return A
    else:
        B = []
        while len(B) != n:
            j = 0
            for i in range(len(A) - 1):
                if A[j] > A[i + 1]:
                    j = i + 1
            B.append(A.pop(j))
        return B


if __name__ == "__main__":
    A = []
    print(f"tri_selection({A}) = {tri_selection(A)}")
    print("\n")
    A = [-8]
    print(f"tri_selection({A}) = {tri_selection(A)}")
    print("\n")
    A = [1, -8, 45]
    print(f"tri_selection({A}) = {tri_selection(A)}")
    print("\n")
    A = [-100, -1000, 0, -0, -7, -7, 12, -2000]
    print(f"tri_selection({A}) = {tri_selection(A)}")
