def tri_insertion(A):
    n = len(A)
    if n < 2:
        return A
    else:
        for i in range(1, len(A)):
            j = i
            while j != 0:
                j -= 1
                if A[j] > A[j + 1]:
                    Permute = A[j]
                    A[j] = A[j + 1]
                    A[j + 1] = Permute
        return A


if __name__ == "__main__":
    A = []
    print(f"tri_insertion({A}) = {tri_insertion(A)}")
    print("\n")
    A = [-8]
    print(f"tri_insertion({A}) = {tri_insertion(A)}")
    print("\n")
    A = [1, -8, 45]
    print(f"tri_insertion({A}) = {tri_insertion(A)}")
    print("\n")
    A = [-100, -1000, 0, -0, -7, -7, 12, -2000]
    print(f"tri_insertion({A}) = {tri_insertion(A)}")
